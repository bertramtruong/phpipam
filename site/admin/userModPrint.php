<?php

/**
 * Script to print add / edit / delete users
 *************************************************/

/* required functions */
require_once('../../functions/functions.php'); 

/* verify that user is admin */
checkAdmin();

/* get all settings */
$settings = getAllSettings();
?>


<!-- header -->
<div class="pHeader">
<?php
/**
 * If action is not set get it form post variable!
 */
if (!$action) {
    $action = $_POST['action'];
    $id     = $_POST['id'];
    
    //fetch all requested userdetails
    $user = getUserDetailsById($id);
    
    if(!empty($user['real_name'])) 	{ print ucwords($action) .'  user '. $user['real_name']; }
    else 							{ print 'Add new user'; }
}
else {
	/* Set dummy data  */
	$user['real_name'] = '';
	$user['username']  = '';
	$user['email']     = '';
	$user['password']  = '';
	
	print 'Add new user';
}
?>
</div>


<!-- content -->
<div class="pContent">

	<form id="userMod" name="userMod">
	<table class="userMod table table-noborder table-condensed">

	<!-- real name -->
	<tr>
	    <td>Real name</td> 
	    <td><input type="text" name="real_name" value="<?php print $user['real_name']; ?>"></td>
       	<td class="info">Enter users real name</td>
    </tr>

    <!-- username -->
    <tr>
    	<td>Username</td> 
    	<td><input type="text" name="username" value="<?php print $user['username']; ?>" <?php if($action == "Edit") print 'readonly'; ?>></td>   
    	<td class="info">Enter username</td>
    </tr>

    <!-- email -->
    <tr>
    	<td>e-mail</td> 
    	<td><input type="text" name="email" value="<?php print $user['email']; ?>"></td>
    	<td class="info">Enter users email address</td>
    </tr>

<!-- type -->
<?php
/* if domainauth is not enabled default to local user */
if($settings['domainAuth'] == 0) {
	print '<input type="hidden" name="domainUser" value="'. $user['domainUser'] .'">'. "\n";
}
else {

	print '<tr>'. "\n";
    print '	<td>User Type</td> '. "\n";
    print '	<td>'. "\n";
    print '	<select name="domainUser" id="domainUser">'. "\n";
    print '	<option value="0" '. "\n";
    		if ($user['domainUser'] == "0") print "selected"; 
    print '	>Local user</option>'. "\n";
    print '	<option value="1" '. "\n"; 
    		if ($user['domainUser'] == "1") print "selected"; 
    print '	>Domain user</option> '. "\n";
    print '	</select>'. "\n";
    print '	</td> '. "\n";
    print '	<td class="info">Set user type'. "\n";
    print '	<ul>'. "\n";
    print '		<li>Local authenticates here</li>'. "\n";
    print '		<li>Domain authenticates on AD, but still needs to be setup here for permissions etc.</li>'. "\n";
    print '	</ul>'. "\n";
    print '	</td>  '. "\n";
	print '</tr>'. "\n";

}
	if ($user['domainUser'] == "1") { $disabled = "disabled"; }
	else 							{ $disabled = ""; }
?>


    <!-- password -->
    <tr class="password">
    	<td>Password</td> 
    	<td><input type="password" class="userPass" name="password1" <?php print $disabled; ?>></td>
    	<td class="info">Users password (<a href="#" id="randomPass">click to generate random!</a>)</td>
    </tr>

    <!-- password repeat -->
    <tr class="password">
    	<td>Password</td> 
    	<td><input type="password" class="userPass" name="password2" <?php print $disabled; ?>></td>   
    	<td class="info">Re-type password</td>
    </tr>

    <!-- send notification mail -->
    <tr>
    	<td>Notification</td> 
    	<td><input type="checkbox" name="notifyUser" <?php if($action == "add") { print 'checked'; } else if($action == "delete" || $action = "edit") { print 'disabled="disabled"';} ?>></td>   
    	<td class="info">Send notification email to user with account details</td>
    </tr>

    <!-- role -->
    <tr>
    	<td>User role</td> 
    	<td>
        <select name="role">
            <option name="admin"    <?php if ($user['role'] == "Administrator") print "selected"; ?>>Administrator</option>
            <option name="operator" <?php if ($user['role'] == "Operator")      print "selected"; ?>>Operator</option>      
            <option name="viewer" 	<?php if ($user['role'] == "Viewer")      	print "selected"; ?>>Viewer</option> 
        </select>
        
        <input type="hidden" name="userId" value="<?php if(isset($user['id'])) { print $user['id']; } ?>">
        <input type="hidden" name="action" value="<?php print $action; ?>">
        
        </td> 
        <td class="info">Select user role
	    	<ul>
		    	<li>Administrator is almighty</li>
		    	<li>Operator can view/edit IP addresses (cannot add section, subnets, modify server settings etc)</li>
		    	<li>Viewer can only view IP addresses</li>
		    </ul>
		</td>  
	</tr>

</table>
</form>

</div>




<!-- footer -->
<div class="pFooter">
	<button class="btn btn-small hidePopups">Cancel</button>
	<button class="btn btn-small" id="editUserSubmit"><i class="icon-gray icon-ok"></i> <?php print ucwords($_POST['action']); ?> user</button>

	<!-- Result -->
	<div class="userModResult"></div>
</div>
