<?php

/*
 * sime search tips....
 **************************/
?>

<h4>Some search tips</h4>
<hr>

<div class="alert alert-block alert-warn">
<ul>

	<!-- text -->
	<li><b>Text search</b>
		<ul>
			<li>Text searches through description, hostname, switch, port and owner</li>
			<li>For wildcard search add * (user*)</li>
		</ul>
	
	</li>
	<br>

	<!-- ipv4 -->
	<li><b>IPv4 search tips</b>
		<ul>
			<li>You can search through ranges with * (e.g. 10.23.3.*)</li>
			<li>* shows ALL IP addresses and subnets</li>
		</ul>
	
	</li>
	<br>
	
	<!-- IPv6 -->
	<li><b>IPv6 search tips</b>
		<ul>
			<li>You can search through ranges by specifying whole subnet (e.g. 2002:8a10::/32)</li>
		</ul>
	
	</li>
	<br>

	<!-- Hostname search-->
	<li><b>Hostname search tips</b>
		<ul>
			<li>You can get all IP addresses some host uses and all ports it is connected to by entering hostname in search field</li>
		</ul>
	
	</li>
	<br>

	<!-- Used switch portsh-->
	<li><b>Switch search tips</b>
		<ul>
			<li>You can get all used / available ports and connected IP's / hostnames in some switch by entering switch name in search field</li>
		</ul>
	
	</li>
	<br>

	<!-- MAC address-->
	<li><b>MAC search tips</b>
		<ul>
			<li>You can search by MAC address list entering MAC in 00:1cd:d4:78:ec:46 or 001dd478ec46 format, or search multiple with 00:1c:c4:</li>
		</ul>
	
	</li>
	<br>

	<!-- Custom fieldss-->
	<li><b>Custom field search tips</b>
		<ul>
			<li>You can search custom fields</li>
		</ul>
	
	</li>

</ul>
</div>